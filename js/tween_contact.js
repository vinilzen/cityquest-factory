var params = {};
var tl = new TimelineMax({paused: true});

function setup() {
	var $eA = $('#hexYellow');
	var $eB = $('#hexRed');
	var $eC = $('#hexBlue');
	var $eD = $('#hexGreen');
	var figures = {
		eA: {
			w: $eA[0].getBBox().width,
			h: $eA[0].getBBox().height
		},
		eB: {
			w: $eB[0].getBBox().width,
			h: $eB[0].getBBox().height
		},
		eC: {
			w: $eC[0].getBBox().width,
			h: $eC[0].getBBox().height
		},
		eD: {
			w: $eD[0].getBBox().width,
			h: $eD[0].getBBox().height
		},
	};

	var $container = $('#contact-page');

	params.w = $container.outerWidth();
	params.h = $container.outerHeight();
	params.dH = $(document).height();
	params.maxScroll = params.dH - $(window).height();


	if ($(window).width() > 767) {
		figures.eA.scale =  (params.h * 0.3)/figures.eA.h;
		figures.eB.scale =  (params.h * 0.3)/figures.eB.h;
		figures.eC.scale =  (params.h * 0.3)/figures.eC.h;
		figures.eD.scale =  (params.h * 0.3)/figures.eD.h;
	} else {
		figures.eA.scale =  (params.h * 0.2)/figures.eA.h;
		figures.eB.scale =  (params.h * 0.2)/figures.eB.h;
		figures.eC.scale =  (params.h * 0.2)/figures.eC.h;
		figures.eD.scale =  (params.h * 0.2)/figures.eD.h;
	}

	$('.animation svg').css({
			width: params.w,
			height: params.h
		}).get(0)
		.setAttribute('viewBox', '0 0 ' + params.w + ' ' + params.h);

	TweenMax.set('svg > *', {transformOrigin:"50% 50%"});
  	tl.clear();

  	var elAXOffset = -(figures.eA.w * figures.eA.scale)/1.6;
  	var elBXOffset = -(figures.eB.w * figures.eB.scale)/1.4;
  	var elCXOffset = params.w - (figures.eC.w * figures.eC.scale)/1.7;
  	var elDXOffset = params.w - (figures.eD.w * figures.eD.scale)/1.7;

  	var elAYOffset = params.h - (figures.eA.h * figures.eA.scale)*1.5;
  	var elBYOffset = params.h - (figures.eB.h * figures.eB.scale)*1.5;
  	var elCYOffset = figures.eC.h * figures.eC.scale/2
  	var elDYOffset = figures.eC.h * figures.eC.scale/2

	TweenMax.set($eA, {x: elAXOffset, y: elAYOffset, scale: figures.eA.scale});
	TweenMax.set($eB, {x: elBXOffset, y: elBYOffset, scale: figures.eB.scale*1.1});
	TweenMax.set($eC, {x: elCXOffset, y: elCYOffset, scale: figures.eC.scale});
	TweenMax.set($eD, {x: elDXOffset, y: elDYOffset, scale: figures.eD.scale});

	$('.animation').css({opacity: 1});

	 tl.addLabel('a')
		 	.to($eA, 1, {x: elAXOffset*1.2, y: elAYOffset*.9, rotation: 200 },'a')
		 	.to($eB, 1, {x: elBXOffset*1.25, y: elBYOffset*.8, rotation: 180},'a')
		 	.to($eC, 1, {x: elCXOffset*0.85, y: elCYOffset*1.1, rotation: 120},'a')
		 	.to($eD, 1, {x: elDXOffset*0.89, y: elDYOffset*1.2, rotation: 150},'a');

	 animate();
}

function animate() {
  if ($(window).width() > 767) {
  	var scroll = $(document).scrollTop();
	  var progress = scroll / params.maxScroll ;
	  if (progress >= 0 && progress <= 2) {
	    tl.seek(progress);
	  }
	}
}

$(window).on("load", function() {
	if ($(window).width() > 767) {
		setup();
		$(window).on('resize', setup);
		$(window).on('scroll', _.throttle(animate, 40));
	}
});
