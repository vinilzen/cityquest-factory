/*
 *  Main JS
 */
var initMap;
$(document).ready(function(){
	$('.carousel-slick').slick({
		centerMode: true,
		centerPadding: '30px',
		adaptiveHeight: true,
		slidesToShow: 1,
		respondTo: 'slider',
		variableWidth: true,
		responsive: [
			{
				breakpoint: 1023,
				settings: {
					centerPadding: '0px',
					respondTo: 'slider',
					variableWidth: true,
					slidesToShow: 1,
				}
			}
		]
	});

	if ($(window).width() < 1024) {
		$('.top-slider').slick({
			responsive: [
				{
					breakpoint: 1023,
					settings: {
						slidesToShow: 3,
					}
				},{
					breakpoint: 767,
					settings: {
						slidesToShow: 2
					}
				},{
					breakpoint: 320,
					settings: {
						slidesToShow: 1
					}
				},
			]
		});
	}

	$('.top-slider-clients').slick({
		slidesToShow: 4,
		slidesToScroll: 4,
		centerPadding: '30px',
		dots: true,
		responsive: [
			{
				breakpoint: 1025,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					dots: false,
				}
			},
			{
				breakpoint: 769,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					dots: false,
				}
			},
		]
	});

	$('[data-toggle="tooltip"]').tooltip()

	var $sliderHandle;

	$( "#slider" ).slider({
		value:2,
		min: 2,
		max: 200,
		step: 1,
		create: function( event, ui ) {
			$sliderHandle = $(".ui-slider-handle");

			$sliderHandle
				.attr('title', '2 000 ₽')
				.tooltip({
					"trigger": 'manual',
					"placement": 'bottom'
				});
				$sliderHandle.tooltip('show');
		},
		slide: function( event, ui ) {
			var val = ui.value +' 000 ₽',
				$sliderTooltip = $('#slider .tooltip'),
				offset = $sliderTooltip.offset(),
				sliderTooltipWidth = $sliderTooltip.width();

			$("#budget").val(ui.value + " ₽");

			$sliderHandle
				.attr({
					'title': val,
					'data-original-title': val
				});

				$('.tooltip-inner').html(val);
		},
		stop: function( event, ui ) {
			$sliderHandle.tooltip('show');
		}
	});

	initMap = function(){
		var styles = [
					{
							"featureType": "water",
							"elementType": "all",
							"stylers": [
									{
											"hue": "#7fc8ed"
									},
									{
											"saturation": 55
									},
									{
											"lightness": -6
									},
									{
											"visibility": "on"
									}
							]
					},
					{
							"featureType": "water",
							"elementType": "labels",
							"stylers": [
									{
											"hue": "#7fc8ed"
									},
									{
											"saturation": 55
									},
									{
											"lightness": -6
									},
									{
											"visibility": "off"
									}
							]
					},
					{
							"featureType": "poi.park",
							"elementType": "geometry",
							"stylers": [
									{
											"hue": "#83cead"
									},
									{
											"saturation": 1
									},
									{
											"lightness": -15
									},
									{
											"visibility": "on"
									}
							]
					},
					{
							"featureType": "landscape",
							"elementType": "geometry",
							"stylers": [
									{
											"hue": "#f3f4f4"
									},
									{
											"saturation": -84
									},
									{
											"lightness": 59
									},
									{
											"visibility": "on"
									}
							]
					},
					{
							"featureType": "landscape",
							"elementType": "labels",
							"stylers": [
									{
											"hue": "#ffffff"
									},
									{
											"saturation": -100
									},
									{
											"lightness": 100
									},
									{
											"visibility": "off"
									}
							]
					},
					{
							"featureType": "road",
							"elementType": "geometry",
							"stylers": [
									{
											"hue": "#ffffff"
									},
									{
											"saturation": -100
									},
									{
											"lightness": 100
									},
									{
											"visibility": "on"
									}
							]
					},
					{
							"featureType": "road",
							"elementType": "labels",
							"stylers": [
									{
											"hue": "#bbbbbb"
									},
									{
											"saturation": -100
									},
									{
											"lightness": 26
									},
									{
											"visibility": "on"
									}
							]
					},
					{
							"featureType": "road.arterial",
							"elementType": "geometry",
							"stylers": [
									{
											"hue": "#ffcc00"
									},
									{
											"saturation": 100
									},
									{
											"lightness": -35
									},
									{
											"visibility": "simplified"
									}
							]
					},
					{
							"featureType": "road.highway",
							"elementType": "geometry",
							"stylers": [
									{
											"hue": "#ffcc00"
									},
									{
											"saturation": 100
									},
									{
											"lightness": -22
									},
									{
											"visibility": "on"
									}
							]
					},
					{
							"featureType": "poi.school",
							"elementType": "all",
							"stylers": [
									{
											"hue": "#d7e4e4"
									},
									{
											"saturation": -60
									},
									{
											"lightness": 23
									},
									{
											"visibility": "on"
									}
							]
					}
			],
			addresses = [
				{"num":0,"lat":55.771509,"lng":37.530445,"id":1}
			],
			markersOptions = {},
			markers = {},
			icon_marker = 'img/redPin.png',
			icon_marker_hover = 'img/redPin.png',
			latlngbounds = new google.maps.LatLngBounds(),
			map;

		$.each(addresses, function(i, v) {
			var latlng = new google.maps.LatLng(v.lat, v.lng);
			latlngbounds.extend(latlng);

			var marker = {
				id: i,
				num: i,
				icon: (i == 0) ? icon_marker_hover : icon_marker,
				position: latlng
			};

			markersOptions[i] = marker;
		});

		var myMapOptions = {
			zoom: 14,
			scrollwheel: false,
			disableDefaultUI: true,
			zoomControl: true,
			zoomControlOptions: {
				style: google.maps.ZoomControlStyle.SMALL,
				position: google.maps.ControlPosition.RIGHT_TOP
			},
			center: latlngbounds.getCenter(), // get center of group items
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			styles:styles
		};

		map = new google.maps.Map(document.getElementById("cqmap"), myMapOptions);

		$.each(markersOptions,function(k,v){
			v.map = map;
			var m = new google.maps.Marker(v);
			markers[k] = m;

			google.maps.event.addListener(m,'click',function(){
				map = m.getMap();
				map.setCenter(m.getPosition()); // set map center to marker position
				smoothZoom(map, 17, map.getZoom()); // call smoothZoom, parameters map, final zoomLevel, and starting zoom level
			});
		});

				// the smooth zoom function
		function smoothZoom (map, max, cnt) {
			if (cnt >= max) {
				return;
			} else {
				z = google.maps.event.addListener(map, 'zoom_changed', function(event){
					google.maps.event.removeListener(z);
					smoothZoom(map, max, cnt + 1);
				});
				setTimeout(function(){map.setZoom(cnt)}, 80); // 80ms is what I found to work well on my system -- it might not work well on all systems
			}
		}
	}

	if ( $('#cqmap').length > 0 ) {
		var google_maps_script = document.createElement('script');
		google_maps_script.setAttribute('src','https://maps.googleapis.com/maps/api/js?key=AIzaSyAJH0jhUnIwe0_OAqdai4zM1_k1M559cv4&callback=initMap&language=en');
		document.body.appendChild(google_maps_script);
	}

	$('a[href*="#"]:not([href="#"])').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');

			var offset = $(window).width()>1023 ? 100 : 0;

			if (target.length) {
				$('html, body').animate({
					scrollTop: target.offset().top - offset
				}, 1000);
				return false;
			}
		}
	});

	var $form = $('#order-form');
	$('#orderModal').on('show.bs.modal', function(e){
		$('#m-navbar').removeClass('in')
		$form.detach();
		$('#orderModal .modal-body').append($form);
	}).on('hidden.bs.modal', function(e){
		$form.detach();
		$('.form-container').append($form);
	});

	$('.navbar-toggle').click(function(){
		var el = $(this).attr('data-target');
		$(el).addClass('in');
	});

	$('.close').click(function(){
		var el = $(this).attr('data-target');
		$(el).removeClass('in');
	});

	var $colQuestHeight = $('.col-quest').first().outerHeight(),
			$quests = $('.quests')
			$questsHeight = $quests.outerHeight();

	function showAllQuests() {
		var h = $questsHeight
		if ($(window).width() < 769 ) {
			h = $colQuestHeight * $('.col-quest').length;
		}

		$('.quests').css({
			'max-height': h
		});
		$(this).one("click", hideAllQuests).text('Hide');
	}

	function hideAllQuests() {
		$('.quests').css({
			'max-height': $colQuestHeight * 2
		});
		$(this).one("click", showAllQuests).text('Show all');
	}

	$('.js-show-all').one('click', showAllQuests);

	$( window ).resize(function() {
		$colQuestHeight = $('.col-quest').first().outerHeight();
		$quests.css({
			'max-height': $colQuestHeight * 2
		});
	});

	$('.btn-js').click(function(){
		$('#orderModal').addClass('order-submited');
		$('.success-submited').fadeIn();
		$('.form-header, .row-form').hide();
		return false;
	});

	$('.js-work').click(function(){
		$('.js-work').removeClass('active');
		var GoToNumber = $(this).data('number');
		$('.carousel-slick').slick('slickGoTo', GoToNumber);
		$(this).addClass('active');
		if ($(window).width() < 768 && $('.row-carousel-work').length > 0) {
			$('html, body').animate({
				scrollTop: $('.row-carousel-work').offset().top //- 60 - 67
			}, 300);
		}
	});

	if ($('body').hasClass('video')) {
		var $h = $('#home');
		var videoSrc = $('body').data('video');
		function set_video_bgr() {
			if (document.body.clientWidth > 1023) {
				var w = $h.outerWidth();
				var $video = $('<div id="video_container" style="display:none"><video autoplay="autoplay" id="bgr_video" loop="loop"></video></div>')
						.prependTo('body.video');

				video = document.getElementById('bgr_video');
				video.addEventListener('loadeddata', function() {

					$h.css('backgroud', 'none');

					var jh = parseInt($h.outerHeight()) +  parseInt($('#items').css("margin-top"));
					var videWidth = '100%';
					var videHeight =  'auto'

					if ($h.outerWidth() / $h.outerHeight() < 1920 / 1080 ) {
						videWidth = 'auto';
						videHeight = jh
					}

					$video
						.css({
							width:'100%',
							height:jh,
							overflow:'hidden',
							position:'absolute',
						})
						.fadeIn(3000)
						.find('video')
						.css({
							// width: '100%',
							// height: '100%',
							width: videWidth,
							height: videHeight,
							'min-width': '100%',
							'min-height': '100%',
							'position': 'absolute',
							'top': '50%',
							'left': '50%',
							'transform': 'translate(-50%,-50%)',
							display: 'block',
						});
				}, false);

				video.src = videoSrc;
				video.load();
			}
		}

		set_video_bgr();
	}

	if ($('#carouselPhoto').length > 0) {
		$('#carouselPhoto').carousel();
		$('#carouselPhoto .left').click(function(){
			$('#carouselPhoto').carousel('prev');
		});
		$('#carouselPhoto .right').click(function(){
			$('#carouselPhoto').carousel('next');
		});
	}

	function checkIfInView($elements) {
		return function() {
			$.each($elements, function() {
				if ($(this).offset().top < (window.innerHeight + window.pageYOffset)) {
					$(this).addClass('in-view');
				} else {
					$(this).removeClass('in-view');
				}
			});
		}
	}

	if ($('.quest-graph').length > 0) {
		$(window).scroll(checkIfInView($('.quest-graph')) );
		checkIfInView($('.quest-graph'))();
	}

	if ($('.container-servives').length > 0) {
		$(window).scroll(checkIfInView($('.container-servives .media')) )
		checkIfInView($('.container-servives .media'))();
	}

	if ($('.quest-page').length > 0) {
		$('.navbar-fixed-top').affix({
			offset: {
				top: function () {
					return (this.top = $('.jumbotron.quest').outerHeight(true))
				}
			}
		})
	}
	
	if ($('.equip-page').length > 0) {
		$('.row-preview-equip a').click(function(){
			var imgSrc = $(this).find('img').data('img');
			var targetImg = $(this).find('img').data('target');
			$('#'+targetImg).attr({'src': imgSrc});
		});
	}

});

function getBoxSize($el) {
  return {
    w: $el[0].getBBox().width,
    h: $el[0].getBBox().height,
  }
}
function getScale(boxW, elW) {
  var scale = (boxW*RATIO)/elW;
  return scale;
}
function scaleEl(path) {
  path.s = getBoxSize(path.el);
  path.scale = getScale(params.w, path.s.w);
  TweenMax.set(path.el, {scale: path.scale});
}
function leftRight(path, i){
	var xOf = -(path.s.w*path.scale*X_OFFSET);
	var yOf = path.s.h*path.scale*(i+1)/2;

	if (i != 0)
		yOf = path.s.h*path.scale*(1+i*0.1)/2;
	
	if (i % 2 === 0)
		xOf = params.w - path.s.w*path.scale*(1-X_OFFSET);

	TweenMax.set(path.el, {x:xOf, y:yOf});
}