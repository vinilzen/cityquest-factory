var params = {};
var tl = new TimelineMax({paused: true});

function setup() {
	var $eR = $('#blobYellowWorks');
	var $eA = $('#blobRedWorks');
	var $eB = $('#blobGreenWorks');
	var figures = {
		eR: {
			w: $eR[0].getBBox().width,
			h: $eR[0].getBBox().height
		},
		eA: {
			w: $eA[0].getBBox().width,
			h: $eA[0].getBBox().height
		},
		eB: {
			w: $eB[0].getBBox().width,
			h: $eB[0].getBBox().height
		},
	};

	var $container = $('#works-page');

	params.w = $container.outerWidth();
	params.h = $container.height();
	params.dH = $(document).height();
	params.maxScroll = params.dH - $(window).height();

	if ($(window).width() > 767) {
		figures.eR.scale =  (params.h * 0.3)/figures.eR.h;
		figures.eA.scale =  (params.h * 0.25)/figures.eA.h;
		figures.eB.scale =  (params.h * 0.23)/figures.eB.h;
		// figures.eR.scale = .1;
		// figures.eA.scale = .1;
		// figures.eB.scale = .1;
	} else {
		figures.eR.scale =  (params.h * 0.1)/figures.eR.h;
		figures.eA.scale =  (params.h * 0.1)/figures.eA.h;
		figures.eB.scale =  (params.h * 0.1)/figures.eB.h;
	}

	$('.animation svg').css({
			width: params.w,
			height: params.h
		}).get(0)
		.setAttribute('viewBox', '0 0 ' + params.w + ' ' + params.h);

	TweenMax.set('svg > *', {transformOrigin:"50% 50%"});
  	tl.clear();

  	var elRedXOffset = -(figures.eR.w * figures.eR.scale)/1.3;
  	var elAquaXOffset = params.w - (figures.eA.w*figures.eA.scale)/3.5;
  	var elBlueXOffset = params.w - (figures.eB.w * figures.eB.scale)/1.9;

	TweenMax.set($eR, {x: elRedXOffset, y: 20, scale: figures.eR.scale*0.8});
	TweenMax.set($eA, {x: elAquaXOffset, y: 380, scale: figures.eA.scale});
	TweenMax.set($eB, {x: elBlueXOffset, y: 200, scale: figures.eB.scale});

	$('.animation').css({opacity: 1});

	tl.addLabel('a')
		 	.to($eR, 1, {x: elRedXOffset*0.7, y: '90%', rotation: 200, scale: figures.eR.scale*0.6},'a')
		 	.to($eA, 1, {x: elAquaXOffset*1.1, y: 1300, rotation: 120},'a')
		 	.to($eB, 1, {x: elBlueXOffset*1.1, y: 1300, rotation: 170},'a');/*
	 	.addLabel('b', '-=1')
			.to($eR, 1, {x: elRedXOffset*0.9, y: '70%', rotation: 360, scale: figures.eR.scale*0.55},'b')
			.to($eA, 1, {x: elAquaXOffset*.9, y: 1400, rotation: 360, scale: .3},'a')
			.to($eB, 1, {x: elBlueXOffset, y: 1400, rotation: 360, scale: .3},'a')
	 	.addLabel('c', '-=0')
			.to($eR, 1, {x: elRedXOffset*0.9, y: '50%', rotation: 10, scale: figures.eR.scale*0.5},'c')
			.to($eA, 1, {x: elAquaXOffset, y: 800, rotation: 5, scale: .3},'b')
			.to($eB, 1, {x: elBlueXOffset, y: 800, rotation: 4},'b');*/

	 animate();
}

function animate() {
  if ($(window).width() > 767) {
	  var scroll = $(document).scrollTop();
	  var progress = scroll / (params.maxScroll*2);
	  if (progress >= 0 && progress <= 2) {
	    tl.seek(progress);
	  }
	}
}

$(window).on("load", function() {
	if ($(window).width() > 767) {
		setup();
		$(window).on('resize', setup);
		$(window).on('scroll', _.throttle(animate, 20));
	}
});
