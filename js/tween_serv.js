var params = {};
var tl = new TimelineMax({paused: true});

function setup() {
	var $eR = $('#eRedServ');
	var $eA = $('#eAquaServ');
	var $eB = $('#eBlueServ');
	var figures = {
		eR: {
			w: $eR[0].getBBox().width,
			h: $eR[0].getBBox().height
		},
		eA: {
			w: $eA[0].getBBox().width,
			h: $eA[0].getBBox().height
		},
		eB: {
			w: $eB[0].getBBox().width,
			h: $eB[0].getBBox().height
		},
	};

	var $container = $('#services-page');

	params.w = $container.outerWidth();
	params.h = $container.height();
	params.dH = $(document).height();
	params.maxScroll = params.dH - $(window).height();

	if ($(window).width() > 767) {
		figures.eR.scale =  (params.h * 0.6)/figures.eR.h;
		figures.eA.scale =  (params.h * 0.52)/figures.eA.h;
		figures.eB.scale =  (params.h * 0.49)/figures.eB.h;
	} else {
		figures.eR.scale =  (params.h * 0.2)/figures.eR.h;
		figures.eA.scale =  (params.h * 0.15)/figures.eA.h;
		figures.eB.scale =  (params.h * 0.15)/figures.eB.h;
	}

	$('.animation svg').css({
			width: params.w,
			height: params.h
		}).get(0)
		.setAttribute('viewBox', '0 0 ' + params.w + ' ' + params.h);

	TweenMax.set('svg > *', {transformOrigin:"50% 50%"});
  	tl.clear();

  	var elRedXOffset = -(figures.eR.w * figures.eR.scale)/2;
  	var elAquaXOffset = params.w - (figures.eA.w*figures.eA.scale)/2.2;
  	var elBlueXOffset = params.w - (figures.eB.w * figures.eB.scale)/2.3;

	TweenMax.set($eR, {x: elRedXOffset, y: 20, scale: figures.eR.scale*0.8});
	TweenMax.set($eA, {x: elAquaXOffset, y: 100, scale: figures.eA.scale});
	TweenMax.set($eB, {x: elBlueXOffset, y: 150, scale: figures.eB.scale});

	$('.animation').css({opacity: 1});

	 tl.addLabel('a')
		 	.to($eR, 1, {x: elRedXOffset*1.2, y: 180, rotation: 120, scale: figures.eR.scale*0.5},'a')
		 	.to($eA, 1, {x: elAquaXOffset/* * .95*/, y: 90, rotation: -9},'c')
		 	.to($eB, 1, {x: elBlueXOffset/**.97*/, y: 110, rotation: -7},'c')
	 	.addLabel('b', '-=1')
			.to($eR, 1, {x: elRedXOffset*1.6, y: 200, rotation: 360, scale: figures.eR.scale*0.55},'b')
			.to($eA, 1, {x: elAquaXOffset/**.98*/, y: 90, rotation: 360},'a')
			.to($eB, 1, {x: elBlueXOffset/**.97*/, y: 110, rotation: 360},'a')
	 	.addLabel('c', '-=0')
			.to($eR, 1, {x: elRedXOffset*1.2, y: 300, rotation: 10, scale: figures.eR.scale*0.4},'c')
			.to($eA, 1, {x: elAquaXOffset/**.98*/, y: 110, rotation: 5},'b')
			.to($eB, 1, {x: elBlueXOffset/**.98*/, y: 140, rotation: 4},'b');

	 animate();
}

function animate() {
  if ($(window).width() > 767) {
	  var scroll = $(document).scrollTop();
	  var progress = scroll / params.maxScroll ;
	  if (progress >= 0 && progress <= 2) {
	    tl.seek(progress);
	  }
	}
}

$(window).on("load", function() {
	if ($(window).width() > 767) {
		setup();
		$(window).on('resize', setup);
		$(window).on('scroll', _.throttle(animate, 40));
	}
});
