var params = {};
var X_OFFSET = 1/2;
var RATIO = 1/3;
var $container = $('#items');
var tl = new TimelineMax({paused: true});

function setup() {
    var Paths = [
        {el:$('#ellipsRed')},
        {el:$('#ellipsYellow')},
        {el:$('#ellipsGreen')}
    ];

    params.w = $container.outerWidth();
    params.h = $container.height();
    params.dH = $(document).height();
    params.maxScroll = params.dH - $(window).height();
    
    if ($(window).width() < 768) {
        RATIO = RATIO/2
    }

    Paths.map(scaleEl)
    Paths.map(leftRight)
    
    $('.animation svg').css({
            width: params.w,
            height: params.h
        }).get(0).setAttribute('viewBox', '0 0 ' + params.w + ' ' + params.h);

    TweenMax.set('svg > *', {transformOrigin:"50% 50%"});
    tl.clear();

    $('.animation').css({opacity: 1});

    tl.addLabel('a')
        .to(Paths[0].el, 1, {rotation: 120},'a')
        .to(Paths[1].el, 1, {rotation: -9},'c')
        .to(Paths[2].el, 1, {rotation: -7},'c')
        .addLabel('b', '-=1')
        .to(Paths[0].el, 1, {rotation: 360},'b')
        .to(Paths[1].el, 1, {rotation: 260},'a')
        .to(Paths[2].el, 1, {rotation: 360},'a')
        .addLabel('c', '-=0')
        .to(Paths[0].el, 1, {rotation: 10},'c')
        .to(Paths[1].el, 1, {rotation: 5},'b')
        .to(Paths[2].el, 1, {rotation: 4},'b');

     animate();
}

function animate() {
  if ($(window).width() > 767) {
      var scroll = $(document).scrollTop();
      var progress = scroll / params.maxScroll * 3;
      if (progress >= 0 && progress <= 2) {
        tl.seek(progress);
      }
    }
}

$(window).on("load", function() {
    if ($(window).width() > 767) {
        setup();
        $(window).on('resize', setup);
        $(window).on('scroll', _.throttle(animate, 40));
    }
});
