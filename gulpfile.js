'use strict';

var gulp 					= require('gulp');
var sass        	= require('gulp-sass');
var pug           = require('gulp-pug');
var sourcemaps 		= require('gulp-sourcemaps');
var autoprefixer 	= require('gulp-autoprefixer');
var browserSync 	= require('browser-sync').create();
var reload      	= browserSync.reload;
var concat        = require('gulp-concat');
var compass       = require('compass-importer');

var iconfont = require('gulp-iconfont');
var iconfontCss = require('gulp-iconfont-css');

// Static server
gulp.task('server', ['iconfont', 'sass', 'pug', 'copy_images', 'copy_svg', 'copy_fonts', 'copy_icon', 'copy_js'], function() {
	browserSync.init({
		reloadDelay: 500,
		notify: false,
		open: false,
		server: {
			baseDir: "./public"
		},
		ghostMode: false
	});

  gulp.watch("./sass/**/*.scss", ['sass']);
  gulp.watch("./js/*.js", ['copy_js']);
	gulp.watch("./pug/*.pug", ['pug']);
	gulp.watch("./public/index.html", reload);
});

gulp.task('sass', function () {

    return gulp.src('./sass/main.scss')
    	.pipe(sourcemaps.init())
    	.pipe(sass({
        importer: compass,
        outputStyle: 'compressed'
      }).on('error', sass.logError))
      .pipe(sourcemaps.write('./', {
            includeContent: false,
            sourceRoot: '../sass'
        }))
      .pipe(gulp.dest('./public/css'))
      .pipe(browserSync.stream());
});

gulp.task('pug', function() {
  return gulp.src('./pug/!(_)*.pug')
    .pipe(pug({
      pretty: true
    }))
    .pipe(gulp.dest('./public'));
});

gulp.task('copy_icon', function () {
  return gulp
    .src(['icons/quote.svg','icons/label.svg','icons/labelBlack.svg','icons/labelTransparent.svg'])
    .pipe(gulp.dest('./public/icons'));
});

gulp.task('copy_images', function () {
  return gulp
    .src('img/*')
    .pipe(gulp.dest('./public/img'));
});

gulp.task('copy_svg', function () {
  return gulp
    .src('svg/*')
    .pipe(gulp.dest('./public/svg'));
});

gulp.task('copy_fonts', function () {
  return gulp
    .src('fonts/**/*')
    .pipe(gulp.dest('./public/fonts'));
});
// gulp.task('copy_icons', function () {
//   return gulp
//     .src('icons/*')
//     .pipe(gulp.dest('./public/icons'));
// });

var fontName = 'cqf';
var runTimestamp = Math.round(Date.now()/1000);
gulp.task('iconfont', function(){
  gulp.src(['icons/*.svg'])
    .pipe(iconfontCss({
      fontName: fontName,
      path: 'sass/tpl/_icons.scss',
      targetPath: '../sass/_icons.scss',
      fontPath: '../fonts/'
    }))
    .pipe(iconfont({
			cssClass: 'icon',
      fontName: fontName,
			normalize: true,
      fontHeight: 1001,
			formats: ['ttf', 'eot', 'woff', 'woff2', 'svg'],
			timestamp: runTimestamp
     }))
    .pipe(gulp.dest('fonts/'));
});

gulp.task('copy_js', function () {
  
  gulp.src('js/tween.js').pipe(gulp.dest('./public/js'));
  gulp.src('js/tween*.js').pipe(gulp.dest('./public/js'));

  return gulp
    .src(['js/jquery.min.js',
          'js/jquery-ui.min.js',
          'js/jquery.ui.touch-punch.min.js',
          'js/jquery.scrollme.min.js',
          'js/bootstrap.min.js',
          'js/slick.min.js',
          'js/main123.js'])
    .pipe(concat('all.js'))
    .pipe(gulp.dest('./public/js'));
});

gulp.task('build', ['sass', 'pug', 'copy_images', 'copy_svg', 'copy_fonts', 'copy_icon', 'copy_js']);

gulp.task('default', ['server']);
