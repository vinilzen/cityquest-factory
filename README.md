# Getting Started

## DEMO

1. [Home Page](http://marchuk.eu/cqf/ru.html)
2. [Quests](http://marchuk.eu/cqf/quests.html)
3. [Quest](http://marchuk.eu/cqf/quest.html)
4. [Works](http://marchuk.eu/cqf/works.html)
5. [Work](http://marchuk.eu/cqf/work.html)
6. [Contacts](http://marchuk.eu/cqf/contact.html)
7. [Services](http://marchuk.eu/cqf/services.html)



## FOR DEVELOPERS

#### 1. Install gulp globally

If you have previously installed a version of gulp globally, please run `npm rm --global gulp`
to make sure your old version doesn't collide with gulp-cli.__

```sh
$ npm install --global gulp-cli
```

#### 2. Initialize your project directory

```sh
$ npm install
```

#### 3. Run gulp

```sh
$ gulp
```