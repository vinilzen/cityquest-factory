Слайд 1

Квесты
Quests
Изделия
Items
Услуги
Services
Кто мы?
About us
Заказать 
Order
Строительство квестов под ключ и производство любых нестандартных изделий
Turnkey quests building and production of any original items
Если вы это придумали, мы это построим
If you can think it up, we can build it
Полный цикл разработки изделий
Full cycle item development
Собственное производство более 500 м2
Own production facility over 500 m2
Доставка в любую точку мира
Worldwide delivery
Более 80 выполненных проектов
Over 80 successful projects
Выполняем все работы под ключ – от проектирование до доставки и монтажа без посредников
We perform all works turnkey style – design to delivery and assembly, without intermediaries
Слайд 2

Квесты под ключ
Turnkey quests
Мы можем отправить вам изделия для самостоятельного монтажа или выехать к вам для пусконаладочных работ
We can send you all the items for standalone assembly or pay you a visit for the commissioning and startup
Квест «Ограбление банка»
The Bank Robbery Quest
от 15 000 $
$15 000 +
Квест «Орден ассасинов»
The Assassins’ Order Quest 
от 25 000 $
$25 000 +
Квест «Зазеркалье»
The Behind the Looking Glass Quest
от 10 000 $
$10 000 +
Квест «Магия кино»
The Cinema Magic Quest
от 35 000 $
$35 000 +
Квест «Книга тайн»
The Book of Mysteries Quest
от 15 000 $
$15 000 +
Квест «Ночь в музее»
The Night in the Museum Quest
от 20 000 $
$20 000 +
Показать все
Show all
Слайд 3

Различные изделия
Various items
В ходе экскурсии по пожарной части вы с друзьями и представить не могли, что из обычных посетителей превратились в настоящих спасателей. Ведь если весь город в огне и все силы брошены на борьбу со стихией, надеяться придется только на свои силы. К счастью, пожарная часть оборудована по последнему слову техники, нужно лишь очень быстро понять, как тут все работает.
During a field trip to a fire station you and your friends could never imagine you’d turn into a real rescue squad. Well, when the whole city is on fire and all the official forces are engaged fighting the calamity, you only have yourselves to rely on. Luckily, the fire station is stocked with state of the art equipment and all you need is to quickly figure out how it all works. 
Слайд 4

Наши услуги
Our services
Создание изделий, декораций и игровых предметов из любых материалов и любой сложности
Production of items, decorations and game objects made of any materials and of any complexity level
Разработка и установка электроники. Мы работаем с большинством платформ с использованием всех типов датчиков и элементов управления
Development and installation of electronics. We work with all major platforms and use all types of sensors and controls
Проектирование и создание дизайна будущих развлечений под конкретное помещение
Design and development of future entertainments to fit specific premises.
Разработка программного обеспечения
Software development.
Создание сценариев квестов, городских и виртуальных квестов, а также абсолютно новых видов развлечений
Creation of quest scripts, urban and virtual quests, as well as absolutely original entertainment types.
Монтаж готовых изделий и пусконаладочные работы на локации
Assembly of final products, commissioning and startup works on site
Слайд 5

Кто мы
About us
CityQuest Factory проект компании CityQuest, одного из лидеров рынка новых развлечений.
CityQuest Factory is a CityQuest project, a leader of the new entertainment market.
Открыто и строится сейчас
Open and under construction
Планируется к открытию в 2016 году
Scheduled for opening in 2016
Слайд 6

Мы обладаем необходимыми мощностями для создания развлечений любой сложности
We have the production capacity required to create entertainments of any complexity level
Собственная производственная площадка в центре Москвы
Own production facility in the center of Moscow
Все необходимое оборудование, включая станки ЧПУ
All the required equipment, including the programmable machine tools
30 специалистов, включая сценаристов, архитекторов, инженеров, бутафоров, декораторов, программистов и дизайнеров
30 specialists, including script writers, architects, engineers, props masters, decorators, software developers and designers
Мы управляем развлечениями на всех этапах, поэтому знаем, как строить
We manage all stages of entertainment, so we know how to build them
Мы можем создать развлечение под ключ, а также решить отдельные задачи.
We can create turnkey entertainment or solve standalone tasks
Слайд 7

Заказать
Place an order
Имя
Name
Бюджет
Budget
Описание заказа
Order description
Заказать
Order
Слайд 8

Контакты
Contacts
г. Москва, ул. Летниковская, д. 4, стр. 2
Moscow, 4 Letnikovskaya st., bld. 2
ВНИМАНИЕ! Правый нижний угол этого слайда:
Powerd by CityQuest
ДОЛЖНО БЫТЬ 
Powered by CityQuest 





